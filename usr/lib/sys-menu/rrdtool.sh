#!/bin/sh

echo "Start time (default: now - 10s)"
echo "[HH.MM DD.MM.YYYY | epoche | -7d, ...]"
read -p "Start time? "
read -p "Step? (default: 300 seconds) "
echo 
read -p "How many Data Sources? " DSnumber

i=1
while [ $i -le $DSnumber ]; do
 read -p "Data Source $i name: "
 read -p "Data Source $i type [GAUGE|COUNTER|DERIVE|ABSOLUTE|COMPUTE]: " DStype
 case $DStype in
  [GAUGE,COUNTER,DERIVE,ABSOLUTE])
	read -p "Heartbeat (suggest 2 x step): "
	read -p "Min value: "
	read -p "Max value: "
	;;
  [COMPUTE])
	read -p "rpn-expression: "
	;;
 esac
 let i=i+1
done

echo 
echo "RRA:type:unknowns:steps:rows"
read -p "How many archive streams? (RRA's) " RRAnumber
i=1
while [ $i -le $RRAnumber ]; do
 read -p "RRA $i consolidation function [AVERAGE|MIN|MAX|LAST]: "
 read -p "RRA $i ratio of unknowns allowed (suggest 0.5): "
 read -p "RRA $i steps: "
 read -p "RRA $i rows:  "
 let i=i+1
done


