# sys-menu
Linux system shell menu for terminals.

Requirements
-----------
* pdmenu
* mc

Installation
------------
Download the git zip file and extract files.  
Copy the usr/* files to /usr/

Removal
-------
Remove:
* /usr/bin/menu
* /usr/lib/sys-menu
* /usr/share/doc/sys-menu

License
-------
See LICENSE.txt

