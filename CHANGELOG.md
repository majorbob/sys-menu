Changelog - sys-menu
====================
2024-05-28 - see git log
2021-05-21 - Added backup & restore for MySQL
2021-02-27 - LXC tweaks
2021-02-09 - Added InfluxDB
2020-12-22 - Added update-exim4.conf
2020-05-20 - Certbot added. Tweaks to Apache & Fail2ban.
2020-04-17 - Added unban option to Fail2ban
2020-03-17 - Kernel booted command line
2019-11-13 - Greylistd locations updated. Duplicate changelogs removed.
2019-07-22 - Fail2ban + UFW updates
2019-07-13 - Git added stash
2019-07-12 - added Snort
2019-07-01 - added Mail
2019-06-25 - updates to Spamassassin and UFW
2019-06-24 - added ss for network listening ports
2019-06-24 - updates to Apache2
2019-06-24 - updates to Greylistd
2019-06-19 - Added Logrotate
2019-06-18 - Added sa-learn to Spamassassin  
2019-06-15 - Git & Avahi changes  
2019-06-15 - Merge branch 'master' of https://gitlab.com/majorbob/sys-menu  
2019-06-15 - Eh?  
2019-06-15 - Fixes to LVM, MDADM & Sqlite3  
2019-06-15 - Git fixes  
2019-06-15 - Added Memory to Info  
2018-12-25 - Exim and Squid updates  
2018-12-07 - Exim updates  
2018-08-25 - Docker added  
2018-07-30 - Git changes  
2018-07-30 - Updated git  
2018-07-30 - Added hostapd  
2018-05-04 - Fail2Ban updates  
2018-05-04 - Added Fail2Ban  
2018-04-26 - PostgreSQL update  
2018-04-25 - PostgreSQL  
2017-11-30 - Exim version; Apt logs  
2017-11-30 - Git changes  
2017-10-03 - Added rkhunter  
2017-09-12 - Added stats to dnsmasq  
2017-09-11 - Fixed network listening ports check  
2017-09-11 - Fixes for dnsmasq  
2017-08-25 - Added username to status bar  
2017-08-09 - DNS fixes  
2017-08-09 - DNS tweaks  
2017-08-09 - Version update  
2017-08-09 - Udev + WOL updates  
2017-07-31 - Added MiniDLNA  
2017-07-25 - Libvirt tweak  
2017-07-25 - Libvirt added  
2017-07-10 - MySQL updates  
2017-07-08 - systemd menu improvements  
2017-07-01 - fix  
2017-07-01 - CHANGELOG update  
2017-07-01 - Git CHANGELOG fix  
2017-07-01 - Git CHANGELOG.md fix  
2017-07-01 - Git fixes  
2017-06-30 - Git fixes  
2017-06-30 - Changelog update  
2017-06-30 - Git fixes  
2017-06-30 - MySQL - added Run As root  
2017-06-19 - Added dnsmasq  
2017-06-18 - smbstatus  
2017-06-18 - Samba updates  
2017-05-31 - cpuinfo  
2017-05-25 - Added Disk info  
2017-05-05 - Added sysctl  
2017-02-13 - Date & Time changes  
2017-02-07 - mysql table update  
2017-02-07 - Exim4 update  
2017-02-07 - Added polkit  
2017-01-08 - Added spamassassin.pdm  
2017-01-08 - Added spamassassin  
2016-12-31 - updated Date & Time menu  
2016-12-31 - Updated Date & Time menu.  
2016-08-14 - Merge pull request #2 from majorbob/dev  
2016-08-14 - Bump  
2016-08-14 - Doc changes  
2016-08-14 - Documentation update  
2016-08-14 - Testing line breaks  
2016-08-14 - Doc testing  
2016-08-14 - Minor change  
2016-08-14 - git fixes  
2016-08-12 - Merge pull request #1 from majorbob/dev  
2016-08-12 - git config  
2016-08-12 - git update  
2016-08-12 - git changes  
2016-08-12 - Added git,lighttpd,openssl  
2016-07-29 - Moved config files to config/sys-menu  
2016-07-29 - Journalctl logs added  
2016-06-30 - Systemd update  
2016-06-30 - Main lines restored (54)  
2016-06-30 - Systemd update  
2016-06-10 - Plymouth changes  
2016-05-30 - Added Power Management  
2016-05-19 - Mdadm update  
2016-05-13 - Updates to apparmor,mdadm & systemd  
2015-11-29 - Added CD-Writer  
2015-11-08 - Initrd menu added.  
2015-11-08 - MySQL authentication fixes.  
2015-11-02 - Fixed Update from Github.  
2015-11-01 - Removed license references from .pdm files.  
2015-11-01 - Update method changed to Github.  
2015-11-01 - First commit  
2015-11-01 - Create LICENSE.txt  
2015-11-01 - Delete LICENSE  
2015-10-29 - Update README.md  
2015-10-29 - Initial commit  
